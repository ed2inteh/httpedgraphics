create database intehwork;
use intehwork;

DROP TABLE IF EXISTS `DutyJournal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DutyJournal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `officeWorker` varchar(255) DEFAULT NULL,
  `homeWorker` varchar(255) DEFAULT NULL,
  `dayNumber` int(11) NOT NULL,
  `workDate` DATE,
  `isHoliday` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=408025 DEFAULT CHARSET=utf8;



CREATE TABLE `Workers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=408025 DEFAULT CHARSET=utf8;