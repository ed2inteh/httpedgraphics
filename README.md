# httpedgraph

Микросервис для сбора информации о выпуске товаров на ВХ

## Установка

Для работы сервиса необходимо установить программу Docker. Установка Docker проводится согласно [официальной инструкции](https://docs.docker.com/install/linux/docker-ce/ubuntu/). 
Также необходимо установить Docker Compose, следуя [официальной инструкции](https://docs.docker.com/compose/install/). В завершение необходимо перезапустить демона Docker командой `sudo pkill -SIGHUP dockerd`. Для развёртывания на удалённой машине необходимо установить программу Docker Machine согласно [официальной инструкции](https://docs.docker.com/machine/install-machine/).

## Использование

Запуск пакета программ осуществляется с помощью программы Docker Compose, используя `yml` файлы из корневой директории проекта. Основным является файл `docker-compose.yml`, а остальные файлы используются для изменения либо дополнения функционала, описанного в основном файле. Например, для запуска проекта в отладочном режиме необходимо выполнить следующую команду:
```bash
cd httpedgraph
docker-compose -f docker-compose.yml up
```

Для развёртывания на удалённом сервере необходимо сгенерировать ssh ключ и добавить его на удалённый сервер:
```bash
ssh-keygen -t rsa
ssh-copy-id -i .ssh/id_rsa.pub username@xx.xx.xx.xx
```
, где `username` и `xx.xx.xx.xx` - это имя пользователя и IP адрес удалённого сервера соответственно. Также необходимо обеспечить пользователю на удалённой машине выполнение команд `sudo` без пароля. Для этого необходимо добавить следующую строку в файл `/etc/sudoers` на удалённом сервере:
```
username ALL=(ALL) NOPASSWD:ALL
```
Далее нужно добавить конфигурацию удалённого сервера с помощью программы Docker Machine на рабочей машине:
```bash
docker-machine create \
--driver generic \
--generic-ip-address=192.168.0.130 \
--generic-ssh-key ~/.ssh/httpedgraph \
--generic-ssh-user mixa \
httpedgraph
```
После этого нужно настроить рабочее окружение для запуска контейнеров на
удалённом сервере:
```bash
eval $(docker-machine env httpedgraph)
```
С этого момента все команды консольной утилиты `docker` будут выполняться на удалённой машине. Для возвращения режима работы с локальным демоном Docker нужно выполнить следующую команду:
```bash
eval $(docker-machine env -u)
```

#test url
curl -X GET http://192.168.0.47:5007/addtoken?token=135 --user user:Diw356UT100


docker-compose up




## Авторы

Copyright (c) 2019 Mikhail Butakov