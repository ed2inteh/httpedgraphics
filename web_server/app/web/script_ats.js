var socket = io();
socket.on('callphone', function(data){
  document.getElementById("phone" + data.id).innerHTML = data.phone;
  document.getElementById("contact"+ data.id).innerHTML = data.contact;
  if(data.state == 'DialEnd'){
    document.getElementById("card"+ data.id).style.backgroundColor = "#F9FF91";
  } else if(data.state == 'DialBegin')
  {
    document.getElementById("card"+ data.id).style.backgroundColor = "#B5C6FF";
  }else if(data.state == 'DND_ON')
  {
    document.getElementById("card"+ data.id).style.backgroundColor = "#FF948E";
    document.getElementById("phone" + data.id).innerHTML = "DND";
    document.getElementById("contact"+ data.id).innerHTML = "";
  } 
  else{
    document.getElementById("phone" + data.id).innerHTML = "";
    document.getElementById("contact"+ data.id).innerHTML = "";
    document.getElementById("card"+ data.id).style.backgroundColor = "";
  }

});




