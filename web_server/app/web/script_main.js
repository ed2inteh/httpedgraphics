var panic = { }
var start = 1;

panic['EADD_STATE']  = {"value": 0, "text_bad":"Наблюдаются проблемы с электронным архивом!", "text_ok": "Архивы заработали!" };
panic['PI_STATE']  = {"value": 0, "text_bad":"Наблюдаются проблемы с предварительным информированием!", "text_ok": "Предварительное информирование заработало!" };
panic['OPENPROC_STATE']  = {"value": 0, "text_bad":"Наблюдаются проблемы с открытием процедур!", "text_ok": "Процедуры стали открываться!" };
panic['CONFIRM_STATE']  = {"value": 0, "text_bad":"Перестали приходить подтверждения!", "text_ok": "Подтверждения стали приходить!" };
panic['DECLRECEIVE_STATE']  = {"value": 0, "text_bad":"Декларации не доходят до поста!", "text_ok": "Декларации стали доходить до поста!" };
panic['TRANSIT_STATE']  = {"value": 0, "text_bad":"Не приходят идентификаторы по транзиту!", "text_ok": "Идентификаторы стали приходить!" };


var ctxHealthEADD = document.getElementById('chartHealthEADD')
var chartHealthEADD = new Chart(ctxHealthEADD, {
    type: 'doughnut',
    data: {
        datasets: [    
            {     
                data: [],           
                backgroundColor: ["#00B715", "#FF4949"],
            }
        ]
    },
    options: {
        aspectRatio: 1,
        maintainAspectRatio: false,
        circumference: 180,
        rotation: -90,
        cutoutPercentage: 64,
      plugins: {
          title: {
              display: true,
              text: 'Архивы'
          }
        },
        layout: {
          padding: {
              bottom: -120
          }
      }
  }
  });



  var ctxHealthConfirm = document.getElementById('chartHealthConfirm')
  var chartHealthConfirm = new Chart(ctxHealthConfirm, {
      type: 'doughnut',
      data: {
          datasets: [    
              {     
                  data: [],           
                  backgroundColor: ["#00B715", "#FF4949"], 
                  }
          ]
      },
      options: {
        maintainAspectRatio: false,
        circumference: 180,
        rotation: -90,
        cutoutPercentage: 64,
        plugins: {
            title: {
                display: true,
                text: 'Ответы таможни'
            }
        }
    }
    });

  var ctxHealthCustomsReceive= document.getElementById('chartHealthCustomsReceive')
  var chartHealthCustomsReceive = new Chart(ctxHealthCustomsReceive, {
      type: 'doughnut',
      data: {
          datasets: [    
              {     
                  data: [],           
                  backgroundColor: ["#00B715", "#FF4949"], 
                  }
          ]
      },
      options: {
        maintainAspectRatio: false,
        circumference: 180,
        rotation: -90,
        cutoutPercentage: 64,
        plugins: {
            title: {
                display: true,
                text: 'Подача'
            }
        }
    }
    });

var ctxHealthOpenProc= document.getElementById('chartHealthOpenProc')
  var chartHealthOpenProc = new Chart(ctxHealthOpenProc, {
      type: 'doughnut',
      data: {
          datasets: [    
              {     
                  data: [],           
                  backgroundColor: ["#00B715", "#FF4949"], 
                  }
          ]
      },
      options: {
        maintainAspectRatio: false,
        circumference: 180,
        rotation: -90,
        cutoutPercentage: 64,
        plugins: {
            title: {
                display: true,
                text: 'Процедуры'
            }
        }
    }
    });


var ctxHealthPi= document.getElementById('chartHealthPi')
  var chartHealthPi = new Chart(ctxHealthPi, {
      type: 'doughnut',
      data: {
          datasets: [    
              {     
                  data: [],           
                  backgroundColor: ["#00B715", "#FF4949"], 
                  }
          ]
      },
      options: {
        maintainAspectRatio: false,
        circumference: 180,
        rotation: -90,
        cutoutPercentage: 64,
        plugins: {
            title: {
                display: true,
                text: 'ПИ'
            }
        }
    }
    });

    var ctxHealthTransit= document.getElementById('chartHealthTransit')
    var chartHealthTransit = new Chart(ctxHealthTransit, {
        type: 'doughnut',
        data: {
            datasets: [    
                {     
                    data: [],           
                    backgroundColor: ["#00B715", "#FF4949"], 
                    }
            ]
        },
        options: {
          maintainAspectRatio: false,
          circumference: 180,
          rotation: -90,
          cutoutPercentage: 64,
          plugins: {
              title: {
                  display: true,
                  text: 'Транзит'
              }
          }
      }
      });





//Подача документов в архив
var ctxEADD = document.getElementById('chartEADD').getContext('2d');
var chartEADD = new Chart(ctxEADD, {
    type: 'line',
    data: {
        labels: [],
        datasets: [    
            {     
                data: [],           
                label: 'Ответ', 
                backgroundColor:' #45B5B3',
                borderColor: ' #45B5B3', 
                tension: 0.3,
                borderWidth: 2,
                pointStyle: 'rectRot',
                pointRadius: 1,
            },
            {
                data: [],
                label: 'Ошибки', 
                backgroundColor:' #E7476F',
                borderColor: ' #E7476F',
                tension: 0.35,
                borderWidth: 2,
                pointStyle: 'rectRot',
                pointRadius: 1,
            },
            {
                data: [],
                label: 'Отправка', 
                backgroundColor:' #F7C445',
                borderColor:' #F7C445',
                tension: 0.41,
                borderWidth: 2,
                pointStyle: 'rectRot',
                pointRadius: 1,
            },
        ]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }, 
        plugins: {
            title: {
                display: true,
                text: 'Подача документов в архив'
            }
        }
    }
});

//Подача ДТ
var ctxDECL = document.getElementById('chartDECL').getContext('2d');
var chartDECL = new Chart(ctxDECL, {
    type: 'line',
    data: {
        labels: [],
        datasets: [    
            {     
                data: [],           
                label: 'Запрос на открытие', 
                backgroundColor:' #404040',
                borderColor: ' #404040', 
                tension: 0.3,
                borderWidth: 1,
                pointStyle: 'rectRot',
                pointRadius: 2,
            },
            {
                data: [],
                label: 'Процедура открыта', 
                backgroundColor:' #308FE6',
                borderColor: ' #308FE6',
                tension: 0.35,
                borderWidth: 2,
                pointStyle: 'rectRot',
                pointRadius: 1,
            }
        ]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }, 
        plugins: {
            title: {
                display: true,
                text: 'Открытие процедур'
            }
        }
    }
});

//Подтверждающие сообщения
var ctxCOMFIRM = document.getElementById('chartCOMFIRM').getContext('2d');
var chartDCOMFIRM = new Chart(ctxCOMFIRM, {
    type: 'line',
    data: {
        labels: [],
        datasets: [    
            {     
                data: [],           
                label: 'Таможня', 
                backgroundColor:' #00960F',
                borderColor: ' #00960F', 
                tension: 0.3,
                fill: true,
                borderWidth: 1,
                pointStyle: 'rectRot',
                pointRadius: 1,
            },
            {
                data: [],
                label: 'Шлюз', 
                backgroundColor:' #FF00DC',
                borderColor: ' #FF00DC', 
                tension: 0.3,
                fill: true,
                borderWidth: 1,
                pointStyle: 'rectRot',
                pointRadius: 1,
            },
            {
                data: [],
                label: 'Ошибки', 
                backgroundColor:' #7F0000',
                borderColor: ' #7F0000', 
                tension: 0.3,
                fill: true,
                borderWidth: 1,
                pointStyle: 'rectRot',
                pointRadius: 1,
            }
        ]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }, 
        plugins: {
            title: {
                display: true,
                text: 'Подтверждения'
            }
        }
    }
});

//Подача ПИ
var ctxPITranzit = document.getElementById('chartPITranzit').getContext('2d');
var chartPITranzit = new Chart(ctxPITranzit, {
    type: 'line',
    data: {
        labels: [],
        datasets: [    
            {     
                data: [],           
                label: 'ПИ', 
                backgroundColor:' #404040',
                borderColor: ' #404040', 
                tension: 0.3,
                borderWidth: 1,
                pointStyle: 'rectRot',
                pointRadius: 2,
            },
            {
                data: [],
                label: 'Рег ПИ', 
                backgroundColor:' #308FE6',
                borderColor: ' #308FE6',
                tension: 0.35,
                borderWidth: 2,
                pointStyle: 'rectRot',
                pointRadius: 1,
            },
            {     
                data: [],           
                label: 'Транзита', 
                backgroundColor:' #FF6A00',
                borderColor: ' #FF6A00', 
                tension: 0.3,
                borderWidth: 1,
                pointStyle: 'rectRot',
                pointRadius: 2,
            },
            {
                data: [],
                label: 'УИН', 
                backgroundColor:' #00990F',
                borderColor: ' #00990F',
                tension: 0.35,
                borderWidth: 2,
                pointStyle: 'rectRot',
                pointRadius: 1,
            }
        ]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }, 
        plugins: {
            title: {
                display: true,
                text: 'ПИ и Транзит'
            }
        }
    }
});


//Подача ДТ
var ctxPodan = document.getElementById('chartPodan').getContext('2d');
var chartPodan = new Chart(ctxPodan, {
    type: 'line',
    data: {
        labels: [],
        datasets: [    
            {     
                data: [],           
                label: 'Первичка', 
                backgroundColor:' #7C7FFF',
                borderColor: ' #7C7FFF', 
                tension: 0.6,
                borderWidth: 1,
                pointStyle: 'rectRot',
                pointRadius: 2,
            },
            {
                data: [],
                label: 'Подача', 
                backgroundColor:' #FFD800',
                borderColor: ' #FFD800',
                tension: 0.35,
                borderWidth: 2,
                pointStyle: 'rectRot',
                pointRadius: 1,
            },
            {
                data: [],
                label: 'Регистрация ДТ', 
                backgroundColor:' #00990F',
                borderColor:' #00990F',
                tension: 0.45,
                borderWidth: 2,
                pointStyle: 'rectRot',
                pointRadius: 1,
            },
        ]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }, 
        plugins: {
            title: {
                display: true,
                text: 'Подача ДТ'
            }
        }
    }
});


//Круговая регистрация
var ctxREGOUT = document.getElementById('chartREGOUT').getContext('2d');
var chartREGOUT = new Chart(ctxREGOUT, {
    type: 'polarArea',
    showTooltips: false,
    data: {
        labels: ['ПЕРВ', 'РЕГ', 'ПОДАЧА'],
        datasets: [    
            {     
                data: [],
                backgroundColor: ['#7C7FFF', '#00990F', '#FFD800'] 
            },
        ]
    },
    options: { 
        plugins: {
            title: {
                display: true,
                text: 'Соотношение регистраций, первичек и подач за 30 минут'
            },
            labels: {
                position: 'outside',
                render: (args) => {
                  return `${args.label}: ${args.value}%`;
                }
              }
        }
    }
});

//Круговая выпуски
var ctxEND = document.getElementById('chartEND').getContext('2d');
var chartEND = new Chart(ctxEND, {
    type: 'polarArea',
    showTooltips: false,
    data: {
        labels: ['ИМ', 'ЭК'],
        datasets: [    
            {     
                data: [],
                backgroundColor: ['#F7C445', '#BE96F8'] 
            },
        ]
    },
    options: { 
        plugins: {
            title: {
                display: true,
                text: 'Выпуски за 30 минут'
            },
            labels: {
                position: 'outside',
                render: (args) => {
                  return `${args.label}: ${args.value}%`;
                }
              }
        }
    }
});



var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onreadystatechange = function() {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            if(xhr.status === 200)
            {
                callback(xhr.response);
            } else
            {
                console.log(xhr.status);
            }
        }
    }
    xhr.send();
};

GraphIntervalFunc();

function GraphIntervalFunc() {
    getJSON('../maindata',
    function(intdata) {
        try {
        chartEADD.data.labels = intdata["date"].map(x => moment(x).format('h:mm')).reverse();
        chartEADD.data.datasets[0].data = intdata["RESULT_EADD"].map(x => x*(-1)).reverse();
        chartEADD.data.datasets[1].data = intdata["ERROR_EADD"].reverse();
        chartEADD.data.datasets[2].data = intdata["SEND_EADD"].reverse();
        chartEADD.update();

        chartDECL.data.labels = intdata["date"].map(x => moment(x).format('h:mm')).reverse();
        chartDECL.data.datasets[0].data = intdata["SEND_OPENPROC"].reverse();
        chartDECL.data.datasets[1].data = intdata["RESULT_OPENPROC"].map(x => x*(-1)).reverse();
        chartDECL.update();

        chartDCOMFIRM.data.labels = intdata["date"].map(x => moment(x).format('h:mm')).reverse();
        chartDCOMFIRM.data.datasets[0].data = intdata["CONFIRM"].reverse();
        chartDCOMFIRM.data.datasets[1].data = intdata["GATEWAY_OK"].map(x => x*(-1)).reverse();
        chartDCOMFIRM.data.datasets[2].data = intdata["GATEWAY_ERR"].reverse();
        
        chartDCOMFIRM.update();

        var newTDArray = [];
        for(let i = 0; i < intdata["TRANSIT_SEND"].length; i++){
            newTDArray[i] = intdata["TRANSIT_SEND"][i] + intdata["TRANSIT_PTD_SEND"][i];
        }

        chartPITranzit.data.labels = intdata["date"].map(x => moment(x).format('h:mm')).reverse();
        chartPITranzit.data.datasets[0].data = intdata["SEND_PI"].reverse();
        chartPITranzit.data.datasets[1].data = intdata["REGISTRATION_PI"].map(x => x*(-1)).reverse();
        chartPITranzit.data.datasets[2].data = newTDArray.reverse();
        chartPITranzit.data.datasets[3].data = intdata["TRANSIT_UIN"].map(x => x*(-1)).reverse();
        chartPITranzit.update();

        chartPodan.data.labels = intdata["date"].map(x => moment(x).format('h:mm')).reverse();
        var newArray = [];
        for(let i = 0; i < intdata["EK_FIRST_DECL"].length; i++){
            newArray[i] = intdata["EK_FIRST_DECL"][i] + intdata["IMP_FIRST_DECL"][i];
        }
        chartPodan.data.datasets[0].data = newArray.reverse();
        chartPodan.data.datasets[1].data = intdata["CUSTOMS_RECEICVE_DECL"].map(x => x*(-1)).reverse();
        chartPodan.data.datasets[2].data = intdata["REGISTRATION_DECL"].reverse();
        chartPodan.update();

        chartREGOUT.data.datasets[0].data = [intdata["EK_FIRST_DECL"].reduce((a,b)=>a+b) + intdata["IMP_FIRST_DECL"].reduce((a,b)=>a+b), intdata["REGISTRATION_DECL"].reduce((a,b)=>a+b), intdata["CUSTOMS_RECEICVE_DECL"].reduce((a,b)=>a+b)]
        chartREGOUT.update();

        chartEND.data.datasets[0].data = [intdata["IMP_END_DECL"].reduce((a,b)=>a+b), intdata["EK_END_DECL"].reduce((a,b)=>a+b)]
        chartEND.update();

        chartHealthEADD.data.datasets[0].data = [intdata["EADD_HEALTH"][0], 100-intdata["EADD_HEALTH"][0]];
        chartHealthEADD.update();

        chartHealthConfirm.data.datasets[0].data = [intdata["CONFIRM_HEALTH"][0], 100-intdata["CONFIRM_HEALTH"][0]];
        chartHealthConfirm.update()


        chartHealthCustomsReceive.data.datasets[0].data = [intdata["DECLRECEIVE_HEALTH"][0], 100-intdata["DECLRECEIVE_HEALTH"][0]];
        chartHealthCustomsReceive.update()

        chartHealthOpenProc.data.datasets[0].data = [intdata["OPENPROC_HEALTH"][0], 100-intdata["OPENPROC_HEALTH"][0]];
        chartHealthOpenProc.update()

        chartHealthPi.data.datasets[0].data = [intdata["PI_HEALTH"][0], 100-intdata["PI_HEALTH"][0]];
        chartHealthPi.update()        
        
        chartHealthTransit.data.datasets[0].data = [intdata["TRANSIT_HEALTH"][0], 100-intdata["TRANSIT_HEALTH"][0]];
        chartHealthTransit.update()


            if(start == 1)
            {
                start = 0;
                for (const [key, value] of Object.entries(panic)) {
                    panic[key].value = intdata[key][0];
                }
            } else
            {
                for (const [key, value] of Object.entries(panic)) {
                    if(panic[key].value != intdata[key][0])
                    {
                        panic[key].value = intdata[key][0];
                        if(panic[key].value == 1)
                        {
                            SayText(panic[key].text_ok, 'zahar');
                        } 
                        else
                        {
                            SayText(panic[key].text_bad, 'zahar');
                        }
                    }
                }  
            }

        }
        catch (e) {
           console.log(e); 
        }
    });
}


setInterval(GraphIntervalFunc, 30000);

