var CTM_NEW = {};

var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onreadystatechange = function() {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            if(xhr.status === 200)
            {
                callback(xhr.response);
            } else
            {
                console.log(xhr.status);
            }
        }
    }
    xhr.send();
};


VersionIntervalFunc();

setInterval(VersionIntervalFunc, 60000);


function VersionIntervalFunc() {
    getJSON('../ctm',
    function(intdata) {
        
        var text = ''
        var rail = new Array(), rj = -1;
        var ved = new Array(), vj = -1;
        var other = new Array(), oj = -1;
        rail[++rj] ='';
        ved[++rj] ='';
        other[++rj] ='';
  

        intdata.forEach(element => {
            if(element['new'] == 'hot')
            {
                text = text + element['name'] + '. Номер версия ' + element['version'] + '. ';  
            } else if(element['new'] == 'today')
            {
                CTM_NEW[element['name']] = element['version'];
            }

            if(element['type'] == 'rail')
            {
                rail[++rj] ='<tr></td><td class="' + element['style'] + '" style="color:#0011BA">';
                rail[++rj] = element['name'];
                rail[++rj] = '</td><td class="' + element['style'] + '">';
                rail[++rj] = '<b>'+element['version']+'</b>';
                rail[++rj] = '</td></tr>';
            } else if(element['type'] == 'ved')
            {
                ved[++rj] ='<tr></td><td class="' + element['style'] + '" style="color: #1B5B00">';
                ved[++rj] = element['name'];
                ved[++rj] = '</td><td class="' + element['style'] + '">';
                ved[++rj] = '<b>'+element['version']+'</b>';
                ved[++rj] = '</td></tr>';
            }
            else if(element['type'] == 'other')
            {
                other[++rj] ='<tr></td><td class="' + element['style'] + '" style="color: #7F0037">';
                other[++rj] = element['name'];
                other[++rj] = '</td><td class="' + element['style'] + '">';
                other[++rj] = '<b>'+element['version']+'</b>';
                other[++rj] = '</td></tr>';
            }

        });
        document.getElementById('ctm').innerHTML = rail.join('') + ved.join('')+ other.join('');

        if (text !='') SayText('Вышло обновление к программе ' + text, 'alyss')
    });
}
