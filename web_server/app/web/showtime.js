
var scheduler = [];
var getDutyEvening = ()=> {
    return `Напоминаю, что сегодня в офисе дежурит с синим телефоном ${ document.getElementById('duty-work').innerHTML }.  Дома дежурит с черным телефоном ${ document.getElementById('duty-home').innerHTML }. Всем хорошего вечера!`
  }
  
  var getDutyMorning = ()=> {
    return `На часах девять утра. Рабочий день начался. Прошу сегодня работать с максимальной отдачей. Согласно расписания в офисе дежурит ${ document.getElementById('duty-work').innerHTML },  дома дежурит ${document.getElementById('duty-home').innerHTML }. Учитывайте эту информацию при планировании своего дня!`
  }
  
  var getHello = ()=> {
    return `Пришел утренний дежурный! Начало рабочего дня.`
  }
  
  
  var getGoHome = ()=> {
    return `Уважаемый ${ document.getElementById('duty-work').innerHTML } можете отправляться домой! Не забывайте закрыть окна.`
  }

  var getBy = ()=> {
    return `Рабочий день почти завершен. Помните про дежурные телефоны! В офисе остается ${ document.getElementById('duty-work').innerHTML }. Дома на телефоне ${document.getElementById('duty-home').innerHTML }. Если пропустите входящий звонок, то вас ждет наказание. Остальные могут отправляться домой, но тоже не расслабляйтесь! Не забывайте включить в телефоне диэнди.`
  }

  var getLunch= ()=> {
    return `Время обеда! Норматив 20 минут.`
  }

  var getLunchEnd= ()=> {
    return `Обед завершен. Приступайте к работе!`
  }

  var getZakaz= ()=> {
    var now = new Date();
    if(now.getDay()==5 || now.getDay()==3)
    {
      return `Группе технической поддержки быть готовыми. Код 17. У вас есть 20 минут на принятие верного решения.`
    } else{
      return `Группе технической поддержки сегодня лучше воздержаться от принятия необдуманных решений по коду 17.`
    }
    
  }


  var getWindowOper= ()=> {
    return `Настало время выполнить проветривание помещений. Время проветривания 2 минуты.`
  }

  var getWindowOperEnd= ()=> {
    return `Хватит проветриваться. Закройте окно. Приступайте к работе.`
  }



  var getNewVersion= () => {
    if(Object.keys(CTM_NEW).length ===0) return '';

    var text = 'Сегодня выходили обновления к программам: ';
    for (const [key, value] of Object.entries(CTM_NEW)) {
      text = text + key + ' версия ' + value.replace('.', ' ') + ', ';
      }
      return text + '. Не забывайте обновится!';   

  }

  var SayNews= () => {
    getJSON('../news',
    function(intdata) {
      var txt = '';
      var cnt = 0;
      intdata.forEach(element => {
        cnt = cnt + 1;
        if(cnt<4)
        {
          txt = txt + element + '.';
        }
      });
      if(txt != '')
      {
        console.log(txt);
        SayText('Новости часа ' + txt, filipp);
      }

    });
  }
  
scheduler[0] = {time:'16:40',  fun: getDutyEvening, flag: 0, voice: 'filipp'};
scheduler[1] = {time:'09:00',  fun: getDutyMorning, flag: 0, voice: 'alena'};
scheduler[2] = {time:'06:40',  fun: getHello, flag: 0, voice: 'alena'};
scheduler[3] = {time:'20:00',  fun: getGoHome, flag: 0, voice: 'alena'};
scheduler[4] = {time:'16:59',  fun: getBy, flag: 0, voice: 'alena'};
scheduler[5] = {time:'12:00',  fun: getLunch, flag: 0, voice: 'filipp'};
scheduler[6] = {time:'13:00',  fun: getLunchEnd, flag: 0, voice: 'filipp'};
scheduler[7] = {time:'08:40',  fun: getNewVersion, flag: 0, voice: 'alena'};
scheduler[8] = {time:'16:55',  fun: getNewVersion, flag: 0, voice: 'alena'};

scheduler[9] =  {time:'10:40',  fun: getWindowOper, flag: 0, voice: 'oksana'};
scheduler[10] =  {time:'10:42',  fun: getWindowOperEnd, flag: 0, voice: 'omazh'};

scheduler[11] =  {time:'13:10',  fun: getWindowOper, flag: 0, voice: 'jane'};
scheduler[12] =  {time:'13:12',  fun: getWindowOperEnd, flag: 0, voice: 'zahar'};

scheduler[13] =  {time:'15:47',  fun: getWindowOper, flag: 0, voice: 'alena'};
scheduler[14] =  {time:'15:49',  fun: getWindowOperEnd, flag: 0, voice: 'ermil'};

scheduler[15] =  {time:'11:02',  fun: getZakaz, flag: 0, voice: 'alena'};

window.onload = function() {
    (function() {
        var date = new Date();
        hours = date.getHours();
        minutes = date.getMinutes();
        seconds = date.getSeconds();
        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}

        var datetime =  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + hours + ":"  + minutes + ":" + seconds;

        time = hours + ':' + minutes;

        for (const element of scheduler) {
            //console.log(element.time + '------');
            if(element.time == time && element.flag ==0 ) {
                element.flag = 1;
                SayText(element.fun(), element.voice)
              }
        }

        document.getElementById('showtime').innerHTML = datetime;
        window.setTimeout(arguments.callee, 1000);
    })();

    SayText('Графики загрузились', 'filipp')
};




var getJSON = function(url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.responseType = 'json';
  xhr.onreadystatechange = function() {
      if (xhr.readyState == XMLHttpRequest.DONE) {
          if(xhr.status === 200)
          {
              callback(xhr.response);
          } else
          {
              console.log(xhr.status);
          }
      }
  }
  xhr.send();
};
