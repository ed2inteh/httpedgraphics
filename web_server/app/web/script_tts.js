//var filename;
var RawFile;

function LoadAndPlayFile(res_file)
{
    var req = new XMLHttpRequest();
    req.open('GET', '/sound/' + res_file + ".raw", true);
    req.overrideMimeType('text\/plain; charset=x-user-defined');
    req.onreadystatechange = function (aEvt) {
        if (req.readyState == 4) {
            if(req.status == 200) {
              PlayRawData(req.responseText);
            } 
        }
    };
    req.send(null);
}

function PlayRawData(sound_raw){
  var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
  var channels = 1;
    var frameCount = sound_raw.length / 2;
    var myAudioBuffer = audioCtx.createBuffer(channels, frameCount, 48000);
    for (var channel = 0; channel < channels; channel++) {

        var nowBuffering = myAudioBuffer.getChannelData(channel,16,48000);
        for (var i = 0; i < frameCount; i++) {
            var word = (sound_raw.charCodeAt(i * 2) & 0xff) + ((sound_raw.charCodeAt(i * 2 + 1) & 0xff) << 8);
            nowBuffering[i] = ((word + 32768) % 65536 - 32768) / 32768.0;
        }
    }
    var source = audioCtx.createBufferSource();
    source.buffer = myAudioBuffer;
    source.connect(audioCtx.destination);
    source.start();
}

var postJSON = function(url,params, callback) {
    var xhr = new XMLHttpRequest();
    xhr.responseType = 'json';
    xhr.open('post', url, true);
    xhr.onreadystatechange = function() {
      if (xhr.readyState == XMLHttpRequest.DONE) {
          if(xhr.status === 200)
          {
              callback(xhr.response);
          } else
          {
              console.log(xhr.status);
          }
      }
    }
    xhr.send(params);
};

function SayText(text, voice)
{
  if(text == '') return;
  if(voice == '') voice = 'alena';
  
  postJSON('../tts', "text=" + text + "&voice="+voice,
      function(intdata) {
        LoadAndPlayFile(intdata['file']);
  });
}



