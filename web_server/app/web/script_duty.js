var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onreadystatechange = function() {
      if (xhr.readyState == XMLHttpRequest.DONE) {
          if(xhr.status === 200)
          {
              callback(xhr.response);
          } else
          {
              console.log(xhr.status);
          }
      }
    }
    xhr.send();
};


HealthIntervalFunc();

function HealthIntervalFunc() {
    getJSON('../duty',
        function(intdata) {
            document.getElementById('duty-work').innerHTML = intdata['office'];
            document.getElementById('duty-home').innerHTML = intdata['home'];
            document.getElementById('duty-work-avatar').src = 'pict/' + intdata['office_avatar'];
            document.getElementById('duty-home-avatar').src = 'pict/' + intdata['home_avatar'];
            //console.log(intdata["e_hour"]["hour"]);
    });
}


setInterval(HealthIntervalFunc, 9000000);