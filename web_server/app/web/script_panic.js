var panic = { }
var start = 1;

panic['EADD_STATE']  = {"value": 0, "text_bad":"Наблюдаются проблемы с элеткронным архивом!", "text_ok": "Архивы заработали!" };
panic['PI_STATE']  = {"value": 0, "text_bad":"Наблюдаются проблемы с предварительным информирование!", "text_ok": "Предварительное информирование заработало!" };
panic['OPENPROC_STATE']  = {"value": 0, "text_bad":"Наблюдаются проблемы с открытием процедур!", "text_ok": "Процедуры стали открываться!" };
panic['CONFIRM_STATE']  = {"value": 0, "text_bad":"Перестали приходить подтверждения!", "text_ok": "Подтверждения стали приходить!" };
panic['DECLRECEIVE_STATE']  = {"value": 0, "text_bad":"Декларации не доходят до поста!", "text_ok": "Декларации стали доходить до поста!" };
panic['TRANSIT_STATE']  = {"value": 0, "text_bad":"Не приходят идентификаторы по транзиту!", "text_ok": "Идентификаторы стали приходить!" };


var ctxHealthEADD = document.getElementById('chartHealthEADD')
var chartHealthEADD = new Chart(ctxHealthEADD, {
    type: 'doughnut',
    data: {
        datasets: [    
            {     
                data: [],           
                backgroundColor: ["#00B715", "#FF4949"],
            }
        ]
    },
    options: {
        aspectRatio: 1,
        maintainAspectRatio: false,
        circumference: 180,
        rotation: -90,
        cutoutPercentage: 64,
      plugins: {
          title: {
              display: true,
              text: 'Архивы'
          }
        },
        layout: {
          padding: {
              bottom: -120
          }
      }
  }
  });



  var ctxHealthConfirm = document.getElementById('chartHealthConfirm')
  var chartHealthConfirm = new Chart(ctxHealthConfirm, {
      type: 'doughnut',
      data: {
          datasets: [    
              {     
                  data: [],           
                  backgroundColor: ["#00B715", "#FF4949"], 
                  }
          ]
      },
      options: {
        maintainAspectRatio: false,
        circumference: 180,
        rotation: -90,
        cutoutPercentage: 64,
        plugins: {
            title: {
                display: true,
                text: 'Ответы таможни'
            }
        }
    }
    });

  var ctxHealthCustomsReceive= document.getElementById('chartHealthCustomsReceive')
  var chartHealthCustomsReceive = new Chart(ctxHealthCustomsReceive, {
      type: 'doughnut',
      data: {
          datasets: [    
              {     
                  data: [],           
                  backgroundColor: ["#00B715", "#FF4949"], 
                  }
          ]
      },
      options: {
        maintainAspectRatio: false,
        circumference: 180,
        rotation: -90,
        cutoutPercentage: 64,
        plugins: {
            title: {
                display: true,
                text: 'Подача'
            }
        }
    }
    });

var ctxHealthOpenProc= document.getElementById('chartHealthOpenProc')
  var chartHealthOpenProc = new Chart(ctxHealthOpenProc, {
      type: 'doughnut',
      data: {
          datasets: [    
              {     
                  data: [],           
                  backgroundColor: ["#00B715", "#FF4949"], 
                  }
          ]
      },
      options: {
        maintainAspectRatio: false,
        circumference: 180,
        rotation: -90,
        cutoutPercentage: 64,
        plugins: {
            title: {
                display: true,
                text: 'Процедуры'
            }
        }
    }
    });


var ctxHealthPi= document.getElementById('chartHealthPi')
  var chartHealthPi = new Chart(ctxHealthPi, {
      type: 'doughnut',
      data: {
          datasets: [    
              {     
                  data: [],           
                  backgroundColor: ["#00B715", "#FF4949"], 
                  }
          ]
      },
      options: {
        maintainAspectRatio: false,
        circumference: 180,
        rotation: -90,
        cutoutPercentage: 64,
        plugins: {
            title: {
                display: true,
                text: 'ПИ'
            }
        }
    }
    });

    var ctxHealthTransit= document.getElementById('chartHealthTransit')
    var chartHealthTransit = new Chart(ctxHealthTransit, {
        type: 'doughnut',
        data: {
            datasets: [    
                {     
                    data: [],           
                    backgroundColor: ["#00B715", "#FF4949"], 
                    }
            ]
        },
        options: {
          maintainAspectRatio: false,
          circumference: 180,
          rotation: -90,
          cutoutPercentage: 64,
          plugins: {
              title: {
                  display: true,
                  text: 'Транзит'
              }
          }
      }
      });


var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
};


HealthIntervalFunc();

function HealthIntervalFunc() {
    getJSON('../health',
        function(err, intdata) {
        if (err !== null) {
            console.log('Get error')
        } else {
            chartHealthEADD.data.datasets[0].data = [intdata["EADD_HEALTH"], 100-intdata["EADD_HEALTH"]];
            chartHealthEADD.update();

            chartHealthConfirm.data.datasets[0].data = [intdata["CONFIRM_HEALTH"], 100-intdata["CONFIRM_HEALTH"]];
            chartHealthConfirm.update()


            chartHealthCustomsReceive.data.datasets[0].data = [intdata["DECLRECEIVE_HEALTH"], 100-intdata["DECLRECEIVE_HEALTH"]];
            chartHealthCustomsReceive.update()

            chartHealthOpenProc.data.datasets[0].data = [intdata["OPENPROC_HEALTH"], 100-intdata["OPENPROC_HEALTH"]];
            chartHealthOpenProc.update()

            chartHealthPi.data.datasets[0].data = [intdata["PI_HEALTH"], 100-intdata["PI_HEALTH"]];
            chartHealthPi.update()        
            
            chartHealthTransit.data.datasets[0].data = [intdata["TRANSIT_HEALTH"], 100-intdata["TRANSIT_HEALTH"]];
            chartHealthTransit.update()


            if(start == 1)
            {
                start = 0;
                for (const [key, value] of Object.entries(panic)) {
                    panic[key].value = intdata[key];
                }
            } else
            {
                for (const [key, value] of Object.entries(panic)) {
                    if(panic[key].value != intdata[key])
                    {
                        panic[key].value = intdata[key];
                        if(panic[key].value == 1)
                        {
                            SayText(panic[key].text_ok, 'alyss');
                        } 
                        else
                        {
                            SayText(panic[key].text_bad, 'alyss');
                        }
                    }
                }  
            }
            
        }
    });
}


