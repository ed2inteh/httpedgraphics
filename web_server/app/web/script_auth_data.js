//Сообщения по часам
var ctxHourMessage = document.getElementById('chartHourMessage').getContext('2d');
var chartHourMessage = new Chart(ctxHourMessage, {
    type: 'bar',
    data: {
        labels: [],
        datasets: [    
            {     
                data: [],           
                label: 'Кол. сообщ.', 
                backgroundColor:' #8C7CFF',
                borderColor: ' #8C7CFF', 
                tension: 0.3,
                borderWidth: 1,
                pointStyle: 'rectRot',
                pointRadius: 2,
            }
        ]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }, 
        plugins: {
            title: {
                display: true,
                text: 'Интенсивность сегодня по часам'
            }
        }
    }
});


//ДТ по РТУ
var ctxRtuDT  = document.getElementById('chartRtuDT').getContext('2d');
var chartRtuDT = new Chart(ctxRtuDT, {
    type: 'bar',
    data: {
        labels: [],
        datasets: [    
            {     
                data: [],           
                label: 'ДТ', 
                backgroundColor:' #46DB02',
                borderColor: ' #46DB02', 
                tension: 0.3,
                borderWidth: 1,
                pointStyle: 'rectRot',
                pointRadius: 2,
            }
        ]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }, 
        plugins: {
            title: {
                display: true,
                text: 'ДТ по РТУ'
            }
        }
    }
});


//Сообщения по таможням
var ctxRtuMessage  = document.getElementById('chartRtuMessage').getContext('2d');
var chartRtuMessage = new Chart(ctxRtuMessage, {
    type: 'bar',
    data: {
        labels: [],
        
        datasets: [    
            {     
                backgroundColor: [],
                data: [],           
                label: 'CMN'
            }
        ]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
            
        }, 
        legend: {
            display: false
        },
        plugins: {
            title: {
                display: true,
                text: 'Сообщения по таможням за час'
            }
        }
    }
});



var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onreadystatechange = function() {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            if(xhr.status === 200)
            {
                callback(xhr.response);
            } else
            {
                console.log(xhr.status);
            }
        }
    }
    xhr.send();
};


HealthIntervalFunc();

function HealthIntervalFunc() {
    getJSON('../seconddata',
        function(intdata) {
            //console.log(intdata["e_hour"]["hour"]);
            chartHourMessage.data.labels = intdata["e_hour"]["hour"];
            chartHourMessage.data.datasets[0].data = intdata["e_hour"]["val"];
            chartHourMessage.update();

            chartRtuDT.data.labels = intdata["e_gtdtu"]["customs"];
            chartRtuDT.data.datasets[0].data = intdata["e_gtdtu"]["val"];
            chartRtuDT.update();
            
            chartRtuMessage.data.labels = intdata["e_rtu"]["customs"];
            chartRtuMessage.data.datasets[0].data = intdata["e_rtu"]["val"];
            chartRtuMessage.data.datasets[0].backgroundColor = intdata["e_rtu"]["colors"];
            
            chartRtuMessage.update();
    });
}


setInterval(HealthIntervalFunc, 30000);

