var request = require('request');
var iconv  = require('iconv-lite');
const fs = require('fs');
let raw_data = fs.readFileSync('./conf/secrets.json');
let secrets = JSON.parse(raw_data);


function Auth(res, fn_oper)
{
     request.post({
        headers: {'content-type' : 'application/x-www-form-urlencoded'},
        url:     'http://infotech.ed2inteh.ru/EDControl/login.php',
        body:    "uName=" + secrets["billing"]["login"]+ "&uPass=" + secrets["billing"]["password"] + "&arch=0"
      }, async function(error, response, body){
        if (!error && response.statusCode == 200) {
          if('set-cookie' in response.headers)
          {
            PHPSESSID = getPHPSESS(response.headers['set-cookie'][0]);   
            sumresult = {};
            for (item of fn_oper){
                await item(PHPSESSID, sumresult);
            }  
            res.write(JSON.stringify(sumresult));
            res.end();
          } 
        } else {
            console.log('error')
        }
      });
}


const e_rtu =  function GetRtuData(PHPSESSID, sumresult)
{
   return  new Promise( (resolve, reject) => {
        request.post({
                headers: {
                    'content-type' : 'application/x-www-form-urlencoded',
                    'Cookie': 'ExtUserName=' + secrets["billing"]["login"]+'; PHPSESSID=' + PHPSESSID+'; _ga=GA1.2.438845471.1563237347;'
                    },
                url:     'http://infotech.ed2inteh.ru/EDControl/e_rtu.php',
                body:    "start=0&limit=50&action=getdata&interval=1%20hour",

            }, function(error, response, body){
                if (!error && response.statusCode == 200) {
                    var result = {};
                    result["customs"] = [];
                    result["val"] = [];
                    result["colors"] = [];
                    let jdata = JSON.parse(body);
                    jdata["Data"].forEach(element => {
                        result["customs"].push(getShortRTU(element['c1']));
                        result["val"].push(element['c2']);
                        if(element['c4'] == '0'){
                            result["colors"].push('#FF6A00');
                        } else{
                            result["colors"].push('#828282');
                        }  
                    });
                    sumresult["e_rtu"] = result;
                    resolve('');
                } else {
                    reject(error);
          }
      });
    });
}


const e_hour = function GetHourData(PHPSESSID, sumresult)
{
    return  new Promise( (resolve, reject) => {

    request.post({
        headers: {
            'content-type' : 'application/x-www-form-urlencoded',
            'Cookie': 'ExtUserName=' + secrets["billing"]["login"]+'; PHPSESSID=' + PHPSESSID+'; _ga=GA1.2.438845471.1563237347;'
            },
        url:     'http://infotech.ed2inteh.ru/EDControl/e_hour.php',
        body:    "start=0&limit=20&action=getdata",
      }, function(error, response, body){
        if (!error && response.statusCode == 200) {
            var result = {};
            var date = new Date();
            result["hour"] = [];
            result["val"] = []; 
            let jdata = JSON.parse(body);
            jdata["Data"].forEach(element => {
                if(element['c0']<date.getHours())
                {   result["hour"].push(element['c0']);
                    result["val"].push(element['c1']);  
                } 
            });
           // console.log(result);
           sumresult['e_hour'] = result;
           
            resolve('');
            } else {
                reject(error);
            }
      });
    });
}


const e_gtdtu = function GetGTDRTUData(PHPSESSID, sumresult)
{
    return  new Promise( (resolve, reject) => {
        request.post({
            headers: {
                'content-type' : 'application/x-www-form-urlencoded',
                'Cookie': 'ExtUserName=' + secrets["billing"]["login"]+'; PHPSESSID=' + PHPSESSID+'; _ga=GA1.2.438845471.1563237347;'
                },
            url:     'http://infotech.ed2inteh.ru/EDControl/e_gtdtu.php',
            body:    "StartDate="+ getSpecCurrDate() +"&StopDate=" + getSpecCurrDate() + "&start=0&limit=20&action=getdata",
        }, function(error, response, body){
                var result = {};
                if (!error && response.statusCode == 200) {
                    result["customs"] = [];
                    result["val"] = []; 
                    let jdata = JSON.parse(body);
                    jdata["Data"].forEach(element => {
                        result["customs"].push(element['c0']);
                        result["val"].push(element['c1']);    
                    });
                    //console.log(result);
                    sumresult['e_gtdtu'] = result;
                
                    resolve('');
                    } else {
                        reject(error);
                    }
        });
    });
}


function getPHPSESS(str)
{
    return str.split(";")[0].split("=")[1];
}

function getSpecCurrDate()
{
    var date = new Date();
    day = date.getDate();
    month = date.getMonth() + 1;
    year = date.getFullYear();
    if (month   < 10) {month   = "0"+month;}
    if (day < 10) {day = "0"+day;}
    return day + '%2F' + month + '%2F' + year.toString().substr(-2);
}

function getShortRTU(tmp){
    const fullName = ['Центральная акцизная таможня','Центральная энергетическая', 'ЦЕНТРАЛЬНОЕ ТАМОЖЕННОЕ УПРАВЛЕНИЕ', 'СЕВЕРО-ЗАПАДНОЕ ТАМОЖЕННОЕ УПРАВЛЕНИЕ', 'ЮЖНОЕ ТАМОЖЕННОЕ УПРАВЛЕНИЕ', 'ПРИВОЛЖСКОЕ ТАМОЖЕННОЕ УПРАВЛЕНИЕ', 'УРАЛЬСКОЕ ТАМОЖЕННОЕ УПРАВЛЕНИЕ',  'СИБИРСКОЕ ТАМОЖЕННОЕ УПРАВЛЕНИЕ', 'ДАЛЬНЕВОСТОЧНОЕ ТАМОЖЕННОЕ УПРАВЛЕНИЕ', 'СЕВЕРО-КАВКАЗСКОЕ ТАМОЖЕННОЕ УПРАВЛЕНИЕ']
    const shortName = ['ЦАТ', 'ЭПОСТ',                             'ЦТУ',                              'СЗТУ',                                      'ЮТУ',                         'ПТУ',                              'УТУ',                        'СТУ', 'ДВТУ', 'СКТУ'];
    for(var i=0 in fullName)
    {
        if(tmp.includes(fullName[i])) return shortName[i];
    }
    return tmp
}
//console.log(getShortRTU('ЦЕНТРАЛЬНОЕ ТАМОЖЕННОЕ УПРАВЛЕНИЕ'));

module.exports = {
    getBillingData: function(res)
    {
        Auth(res, [e_rtu, e_hour, e_gtdtu]); 
    } 
    
}
