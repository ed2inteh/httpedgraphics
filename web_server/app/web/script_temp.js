
var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.timeout = 20000;
    xhr.responseType = 'json';
    xhr.onreadystatechange = function() {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            if(xhr.status === 200)
            {
                callback(xhr.response);
            } else
            {
                console.log(xhr.status);
            }
        }
    }
    xhr.send();
};


function TempIntervalFunc() {
    console.log("Load weather http://api.openweathermap.org/data/2.5/weather?q=Irkutsk&lang=ru&units=metric&appid=62c94a5555080b8d9f7f405adc193c2a");
    getJSON("http://api.openweathermap.org/data/2.5/weather?q=Irkutsk&lang=ru&units=metric&appid=62c94a5555080b8d9f7f405adc193c2a",
    function(intdata) {
        console.log(intdata);
        document.getElementById('showtemp').innerHTML = intdata['main']['temp'] + '℃';
        document.getElementById('showhum').innerHTML = intdata['main']['humidity'] + '%';
        document.getElementById('showwind').innerHTML = intdata['wind']['speed']+ 'км/ч';
}); 


}

setInterval(TempIntervalFunc, 60000);

