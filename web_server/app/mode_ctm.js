var request = require('request');
const { Datastore } = require('nedb-async-await')

function ctm(res)
{
     request({url: 'https://www.ctm.ru'},  async function(error, response, body){
        const db =  Datastore({
            filename: process.env.DATA_PATH +'/ctm',
            autoload: true,
          })
        var result = [];    
         var tmatch = [...body.matchAll('program-name left\">[A-z\\W\\-]*<\\/span><span class=\"program-version right\"><strong>[\\d.]*')];
         for (element of tmatch){
            const tmp = element[0].split('</span><span class="program-version right"><strong>');
            await progs(tmp, result, db); 
         }
        res.write(JSON.stringify(result));
        res.end(); 
      });
}

async function progs(tmp, result, db)
{
    var prog = {};
    prog['name'] = tmp[0].replace('program-name left">', '');
    prog['version'] = tmp[1];  
    if(prog['name'].includes('Rail')){
        prog['type'] = 'rail';
    } else if(prog['name'].includes('ВЭД')){
        prog['type'] = 'ved';
    } else{
        prog['type'] = 'other';
    }  
    await dbsave(prog, db);
    result.push(prog);
}

 async function dbsave(prog, db)
{    
    let rec = await db.findOne({name : prog['name']});
    if(rec == null)
    {
        console.log('add');
        prog['date'] = getCurrDate();
        await db.insert(prog); 
        prog['style'] = '';
        prog['new'] = '';
    } else {
        if(rec.version!=prog['version'])
        {
            console.log('hot ' + prog['name'] + '  ' + rec.version);
            db.update({_id : rec._id}, { $set: { version : prog['version'], date: getCurrDate() } },  {upsert: false});
            prog['style'] = 'table-success';
            prog['new'] = 'hot';
        } else if(rec.date==getCurrDate()){
            prog['style'] = 'table-warning';      
            prog['new'] = 'today'; 
        } 
        else
        {
            prog['style'] = '';
            prog['new'] = ''; 
        }
    }
}

function getCurrDate()
{
    var date = new Date();
    month = date.getMonth();
    day = date.getDate();
    year = date.getFullYear();
    if (month   < 10) {month   = "0"+month;}
    if (day < 10) {day = "0"+day;}
    return day + '.' + month + '.' + year;
}

module.exports = {
    getCTM: function(res)
    {
        ctm(res);
    }
}