
const fs = require('fs');
let raw_data = fs.readFileSync('./conf/secrets.json');
let secrets = JSON.parse(raw_data);
var mysql      = require('mysql');

async function duty(res)
{
  var connection = await mysql.createConnection({
    host     : secrets["mysql"]["server"],
    user     : secrets["mysql"]["login"],
    password : secrets["mysql"]["password"],
    database : secrets["mysql"]["database"]
  });


  await connection.connect();
  var now = new Date();
  var curDate = "'" + now.getFullYear() + "-" + (now.getMonth()+1) + "-" + now.getDate() + "'";
  await connection.query("SELECT officeWorker, homeWorker from DutyJournal WHERE workDate = " + curDate, function (error, results, fields) {
    if (error) throw error;
    output ={};
    output['office'] = results[0].officeWorker;
    output['office_avatar'] = getPict(output['office']);
    output['home']= results[0].homeWorker;
    output['home_avatar'] = getPict(output['home']);
    res.write(JSON.stringify(output));
    res.end();

  });
   
  await connection.end();


}
function getPict(name){
    if (name == 'Михаил Бутаков') return 'avatar_mixa.png'
    if (name == 'Виктор Шодоров') return 'avatar_victor.png'
    if (name == 'Дмитрий Водовозов') return 'avatar_dima_1.png'
    if (name == 'Дмитрий Косов') return 'avatar_dima_2.png'
    return 'avatar_homer.png'
}


module.exports = {
  getDuty: function(res)
  {
    duty(res);
  }
}
