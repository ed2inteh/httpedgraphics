var http = require("http").createServer(handler).listen(3000);
var io = require('socket.io')(http);
var Datastore = require('nedb');
var static = require('node-static');
var file = new(static.Server)(__dirname);
var mode_billing = require('./mode_billing');
var mode_ctm = require('./mode_ctm');
var mode_duty = require('./mode_duty');
var mode_yandex = require('./mode_yandex');
var mode_news= require('./mode_news');
var fs = require('fs');
var { parse } = require('querystring');
var { decode }  = require('iconv-lite');
const { Console } = require("console");
var ami = new require('asterisk-manager')('26677','192.168.0.68','launcher','vhnf4756b', true);
var cnt = 0;


function handler(req, res){
    const { method, url, headers } = req
    if (method === "GET" && url === "/maindata") {
        jheader(res);
        getMainData(res);
    } else if (method === "GET" && url === "/seconddata") {
        jheader(res);
        mode_billing.getBillingData(res);
    } else if (method === "GET" && url === "/ctm") {
        jheader(res);
        mode_ctm.getCTM(res);
    } else if (method === "GET" && url === "/duty") {
        jheader(res);
        mode_duty.getDuty(res);
    } else if (method === "GET" && url === "/news") {
        jheader(res);
        mode_news.getNews(res);
    }else if (method === "POST" && url === "/tts") {
        jheader(res);
        var body = ''
        req.on('data', function(data) {
          body += data.toString(); 
        })
        req.on('end', function() {
          var formData = parse(body);
          mode_yandex.getTTS(res, formData.text, formData.voice); 
        })
    } else {
        file.serve(req, res);
    }   
}


function yebDetector()
{
    var text = ''
    var db = new Datastore({filename : process.env.DATA_PATH + '/ydet'})
    db.loadDatabase()  
    db.find()
    .sort({ date: -1 })  
    .limit(1)
    .exec(function(err, docs) {
        if(docs[0].detect == 1)
        {
            if(cnt == 0)
            {
                var det = {};
                if(docs[0].gender == 'male')
                {
                    det["text"] = 'К нам пришел парень. Его возраст ' + docs[0].age;
                } else
                {
                    det["text"] =  'К нам пришла девушка. Ее возраст ' + docs[0].age;
                }
                console.log(det);
                io.emit('fdetecter', det);
            }
            cnt++;
        } 
        else
        {
            cnt=0;
        }
    });
}

function getMainData(res)
{
        var db = new Datastore({filename : process.env.DATA_PATH + '/ed'})
        db.loadDatabase()  
        db.find()
        .sort({ date: -1 })  
        .limit(30)
        .exec(function(err, docs) {
            var result = {};
            var cnt = 0;
            docs.forEach(element => {
                for(key in element)
                {
                    if((key in result) == false) result[key] = [];
                    result[key].push(element[key])
                }  
            });
            result["cnt"] = range(1, 30);
            res.write(JSON.stringify(result));
            res.end();            
        });
}

function jheader(res)
{
    res.statusCode = 200
    res.setHeader("Content-Type", "application/json")
    // only for CORS
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');  
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');  
    res.setHeader('Access-Control-Allow-Credentials', true);  
}

function range(start, end) {
    return Array(end - start + 1).fill().map((_, idx) => start + idx)
}


ami.keepConnected();
ami.on('managerevent', function(evt) {
    var phone = {};
    if(evt['event'] == 'Newexten' && ['103', '104', '105', '106'].indexOf(evt['calleridnum'])>=0)
    {
        if(evt['appdata'] == 'DB(DND/' +evt['calleridnum']+ ')=')
        {
            console.log(evt);
            phone["id"] = evt['calleridnum'];
            phone["state"] = 'DND_OFF';
            io.emit('callphone', phone);
        }
        if(evt['appdata'] == 'DB(DND/' +evt['calleridnum']+ ')=1')
        {
            console.log(evt);
            phone["id"] = evt['calleridnum'];
            phone["state"] = 'DND_ON';
            io.emit('callphone', phone);
        }

        return;
    }

    if (['DialBegin', 'DialEnd', 'Hangup'].indexOf(evt['event']) < 0 || ['103', '104', '105', '106', '1', ''].indexOf(evt['exten'])<=0) 
    {
        return;
    }
    if(evt['cause-txt'] == 'User busy') return;
    if(evt['dialstatus'] == 'BUSY') return;

    console.log(evt);
    phone["phone"] = evt['calleridnum'];
    phone["contact"] = evt['calleridname'];
    phone["state"] = evt['event'];

    if (evt['event'] == 'DialBegin' && evt['exten'] == '1')
    {
            phone["id"] = evt['dialstring'].split('/')[1];
    } else if (evt['event'] == 'DialEnd' && evt['exten'] == '1')
    {
            phone["id"] = evt['destchannel'].split('/')[1].split('-')[0];
    } else if(evt['event'] == 'Hangup' && (evt['exten'] == '1' || evt['exten'] == ''))
    {
            phone["id"] = evt['channel'].split('/')[1].split('-')[0];
    } else{
        phone["id"] = evt['exten'];
    }
    if (evt['event'] == 'DialBegin'){
        phone["id"] = 'call';
        setTimeout( function() {
            var phone = {};
            phone["phone"] = '';
            phone["contact"] = '';
            phone["id"] = 'call';
            phone["state"] = 'END';
            io.emit('callphone', phone);
          }, 6000);
    }
    io.emit('callphone', phone);
});

function intervalFunc() {
    let date_ob = new Date();
    let hours = date_ob.getHours();
    let minutes = date_ob.getMinutes();
    if(hours == 6 && minutes==38)
    {
        mode_yandex.generateKeyToken();
    }
  }

mode_yandex.generateKeyToken();

setInterval(intervalFunc, 60000);

//setInterval(yebDetector, 3000);
