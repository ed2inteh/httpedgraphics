const fetch = require('node-fetch');
var { post } = require('request');
var fs = require('fs');
let raw_data = fs.readFileSync(process.env.APP_PATH + '/conf/secrets.json');
let secrets = JSON.parse(raw_data);

function GenerateToken(){
    console.log("Generate yandex token")
    var bodykey = {};
    bodykey['yandexPassportOauthToken'] = secrets['yandex']['yandexPassportOauthToken'];
    post(
                {
                headers: {'Content-Type' : 'application/json'},
                url: 'https://iam.api.cloud.yandex.net/iam/v1/tokens', 
                body: JSON.stringify(bodykey),
                },
                async function(error, response, body){
                    console.log(body)
                    fs.writeFile(process.env.APP_PATH + '/conf/yandex_token.json', body, function (err,data) {
                        if (err) {
                          return console.log(err);
                        }
                           console.log(data);
                      });
                });
}

GenerateToken(); // one day run

