const fetch = require('node-fetch');
var { post } = require('request');
var fs = require('fs');
let raw_data = fs.readFileSync('./conf/secrets.json');
let secrets = JSON.parse(raw_data);
var uuid = require('uuid');
const querystring  = require('querystring')
var sox = require('sox');
const { URLSearchParams } = require('url');


function GenerateToken(){
  var bodykey = {};
  bodykey['yandexPassportOauthToken'] = secrets['yandex']['yandexPassportOauthToken'];
  post(
              {
              headers: {'Content-Type' : 'application/json'},
              url: 'https://iam.api.cloud.yandex.net/iam/v1/tokens', 
              body: JSON.stringify(bodykey),
              },
              async function(error, response, body){
                  console.log(body)
                  fs.writeFile(process.env.APP_PATH + '/conf/yandex_token.json', body, function (err,data) {
                      if (err) {
                        return console.log(err);
                      }
                         console.log(data);
                    });
              });
}

//GenerateToken(); // one day run

 function TTS(res, text, voice)
{
   // const { birthtime } = fs.statSync('./conf/yandex_token.json');
   // var d1 = new Date();
   // var d2 = new Date(birthtime);
   // var same = d1.getUTCDate() === d2.getUTCDate();
   // if(same == false)
   // {
   //   GenerateToken();
   // };

    let raw_data = fs.readFileSync('./conf/yandex_token.json');
    let yandex_token = JSON.parse(raw_data);
    const params = new URLSearchParams();
   
    params.append('text', text);
    params.append('voice', voice);
    params.append('emotion', 'good');
    params.append('lang', 'ru-RU');
    params.append('speed', '1.0');
    params.append('format', 'lpcm');
    params.append('folderId', secrets['yandex']['yandex_cloud_folder']);

    var fname = uuid.v1();
    fetch('https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize', {
        method: 'post',        
        body: params,
        headers: {'Authorization': 'Bearer ' + yandex_token['iamToken'],},
    })
    .then(async res_file => { 
        //const dest = fs.createWriteStream('./sound/' + fname+ '.raw');
        //res_file.body.pipe(dest);
        await file_write('./sound/' + fname+ '.raw', res_file);
        result ={};
        result['file'] = fname;
        res.write(JSON.stringify(result));
        res.end();
    })
    .catch(err => console.error(err));
}


const file_write = function (filePath, res_file)
{
    return new Promise((resolve, reject) => {
      const file = fs.createWriteStream(filePath);
      res_file.body.pipe(file);
     // file.end();
      file.on("finish", () => { resolve(); });  
      file.on("error", reject); 
    });
  }

module.exports = {
    getTTS: function(res, text, voice)
    {
        TTS(res, text, voice);
    },
    generateKeyToken: function()
    {
      GenerateToken();
    }
  }




