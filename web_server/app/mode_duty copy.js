const { GoogleSpreadsheet } = require('google-spreadsheet');
const doc = new GoogleSpreadsheet('1hAaGcokuYDKJzXeLKTrLFXITLXpvDH0WQWgvbhfSToI');

const fs = require('fs');
let raw_data = fs.readFileSync('./conf/secrets.json');
let secrets = JSON.parse(raw_data);

async function duty(res)
{
  await doc.useServiceAccountAuth({
      client_email: secrets["google"]["client_email"],
      private_key: secrets["google"]["private_key"]
    });
    await doc.loadInfo();
    const dt = new Date();
    const row = dt.getDate();
    const sheet = doc.sheetsByIndex[0];
    const cl = await sheet.loadCells();
    result ={};
    result['office'] = sheet.getCell(row, 1).value;
    result['office_avatar'] = getPict(result['office']);
    result['home']= sheet.getCell(row, 2).value;
    result['home_avatar'] = getPict(result['home']);
    res.write(JSON.stringify(result));
    res.end();
}
function getPict(name){
    if (name == 'Михаил Бутаков') return 'avatar_mixa.png'
    if (name == 'Виктор Шодоров') return 'avatar_victor.png'
    if (name == 'Дмитрий Водовозов') return 'avatar_dima_1.png'
    if (name == 'Дмитрий Косов') return 'avatar_dima_2.png'
    return 'avatar_homer.png'
}


module.exports = {
  getDuty: function(res)
  {
    duty(res);
  }
}
