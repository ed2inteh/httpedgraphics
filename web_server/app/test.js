
const fs = require('fs');
let raw_data = fs.readFileSync('./conf/secrets.json');
let secrets = JSON.parse(raw_data);
var mysql      = require('mysql');

async function duty()
{
  var connection = await mysql.createConnection({
    host     : secrets["mysql"]["server"],
    user     : secrets["mysql"]["login"],
    password : secrets["mysql"]["password"],
    database : secrets["mysql"]["database"]
  });

  var officeWorker;
  var homeWorker;
  await connection.connect();
  var now = new Date();
  var curDate = "'" + now.getFullYear() + "-" + (now.getMonth()+1) + "-" + now.getDate() + "'";
  await connection.query("SELECT officeWorker, homeWorker from DutyJournal WHERE workDate = " + curDate, function (error, results, fields) {
    if (error) throw error;
    res ={};
    res['office'] = results[0].officeWorker;
    res['office_avatar'] = getPict(res['office']);
    res['home']= results[0].homeWorker;
    res['home_avatar'] = getPict(res['home']);
    console.log(res);
  });
   
  await connection.end();


}

duty();

function getPict(name){
    if (name == 'Михаил Бутаков') return 'avatar_mixa.png'
    if (name == 'Виктор Шодоров') return 'avatar_victor.png'
    if (name == 'Дмитрий Водовозов') return 'avatar_dima_1.png'
    if (name == 'Дмитрий Косов') return 'avatar_dima_2.png'
    return 'avatar_homer.png'
}