const { spawn } = require("child_process");
const canvas = require('canvas');
const faceapi = require('face-api.js');
const fetch = require('node-fetch');
var Datastore = require('nedb');

const { Canvas, Image, ImageData } = canvas
faceapi.env.monkeyPatch({ Canvas, Image, ImageData })
faceapi.env.monkeyPatch({ fetch: fetch });
faceapi.nets.tinyFaceDetector.loadFromDisk(process.env.APP_PATH + '/models');
faceapi.nets.ageGenderNet.loadFromDisk(process.env.APP_PATH + '/models');

async function facer()
{
   data = {};
   data.detect = 0;
   data.gender = '';
   data.age = 0;
   data.date = new Date();
   const img = await canvas.loadImage(process.env.APP_PATH + '/image.jpg');
   const detections = await faceapi.detectAllFaces(img, new faceapi.TinyFaceDetectorOptions()).withAgeAndGender();;
   if(detections.length>0)
   {
       data.detect = 1;
       data.gender = detections[0].gender;
       data.age = detections[0].age | 0;
       console.log(data);
       console.log("FOUND FACE");
   } else
   {
    console.log("no face");
   }
   var db_ed = new Datastore({filename : process.env.DATA_PATH + '/ydet'});
   db_ed.loadDatabase();
   db_ed.insert(data); 
}

function intervalFunc()
{
    console.log("read data");
    const ls = spawn("ffmpeg", ['-y', '-rtsp_transport', 'tcp', '-i', 'rtsp://admin:inteh500919door@192.168.0.99:554/av0_0', '-vframes', '1', process.env.APP_PATH + '/image.jpg']);

    ls.stdout.on("data", data => {
        //console.log(`stdout`);
    });
    
    ls.stderr.on("data", data => {
        //console.log(`stderr: ${data}`);
    });
    
    ls.on('error', (error) => {
      //  console.log(`error: ${error.message}`);
    });
    
    ls.on("close", code => {
        console.log("recognize data");
        facer();
    });
}

setInterval(intervalFunc, 5000);



