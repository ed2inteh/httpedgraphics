var panic = { }

panic['EADD_STATE']  = 1;
panic['EADD_HEALTH'] = 100;
panic['EADD_WAITER'] = 0;

panic['PI_STATE']  = 1;
panic['PI_HEALTH'] = 100;
panic['PI_WAITER'] = 0;

panic['OPENPROC_STATE']  = 1;
panic['OPENPROC_HEALTH'] = 100;
panic['OPENPROC_WAITER'] = 0;


panic['CONFIRM_STATE']  = 1;
panic['CONFIRM_HEALTH'] = 100;
panic['CONFIRM_WAITER'] = 0;

panic['GATEWAY_STATE']  = 1;
panic['GATEWAY_HEALTH'] = 100;
panic['GATEWAY_WAITER'] = 0;

panic['DECLRECEIVE_STATE']  = 1;
panic['DECLRECEIVE_HEALTH'] = 100;
panic['DECLRECEIVE_WAITER'] = 0;

panic['TRANSIT_STATE']  = 1;
panic['TRANSIT_HEALTH'] = 100;
panic['TRANSIT_WAITER'] = 0;

var tempData = {}

module.exports = {
    getHealth:function (storeData)
    {
        //console.log(storeData);
        console.log(tempData);

        if(isEmpty(tempData))
        {
            for (const [key, value] of Object.entries(storeData)) {
                tempData[key] = new Array();
            }
        }
        for (const [key, value] of Object.entries(tempData)) {
            if(tempData[key].length>10) tempData[key].shift();
        }
        for (const [key, value] of Object.entries(storeData)) {
            console.log(key);
            tempData[key].push(storeData[key]);
        }
        console.log(tempData);

        if(isEmpty(storeData)) return;

        getEaddHealth();    
        getPIHealth();    
        getOpenProcHealth();
        getConfirmHealth();
        getGatewayHealth();
        getDeclReceiveHealth();
        getTDHealth();
    
        for (const [key, value] of Object.entries(panic)) {
            storeData[key] = panic[key];
            }
    }
  }

  function isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }

 function getEaddHealth(){
    if(tempData['SEND_EADD'].reduce((a,b)=>a+b) > 0)
    {
        const balanse = tempData['SEND_EADD'].reduce((a,b)=>a+b)  - tempData['RESULT_EADD'].reduce((a,b)=>a+b)  - tempData['ERROR_EADD'].reduce((a,b)=>a+b);
        if(balanse<=0)
        {
            panic['EADD_HEALTH'] = 100 
            panic['EADD_WAITER'] = 0
        } 
        else
        {
            panic['EADD_HEALTH'] = 100 - Math.round(100*Math.abs(balanse)/tempData['SEND_EADD'].reduce((a,b)=>a+b));
        }  
        
        if(panic['EADD_HEALTH']<=10)
        {
            panic['EADD_WAITER'] = panic['EADD_WAITER']  + 1 
            if(panic['EADD_WAITER'] > 2)
            {
                panic['EADD_STATE']  = 0
            } 
        } 
        else
        {
            panic['EADD_STATE']  = 1
            panic['EADD_WAITER'] = 0
        }    
    }
    else if(tempData['RESULT_EADD'].reduce((a,b)=>a+b) > 0)
    {
        panic['EADD_STATE']  = 1;
        panic['EADD_HEALTH'] = 100;
        panic['EADD_WAITER'] = 0
    }
}


 function getPIHealth(){
    if(tempData['SEND_PI'].reduce((a,b)=>a+b) > 0){
        const balanse = tempData['SEND_PI'].reduce((a,b)=>a+b)  - tempData['REGISTRATION_PI'].reduce((a,b)=>a+b);
        if(balanse<=0)
        {
            panic['PI_HEALTH'] = 100 
            panic['PI_WAITER'] = 0
        } 
        else
        {
            panic['PI_HEALTH'] = 100 - Math.round(100*Math.abs(balanse)/tempData['SEND_PI'].reduce((a,b)=>a+b));
        }  
        if(panic['PI_HEALTH']<=10)
        {
            panic['PI_WAITER'] = panic['PI_WAITER'] + 1
            if(panic['PI_WAITER']>3)
            {
                panic['PI_STATE']  = 0;
            }
        } 
        else
        {
            panic['PI_STATE']  = 1;
            panic['PI_WAITER'] = 0;
        }   
    }
    else if(tempData['REGISTRATION_PI'].reduce((a,b)=>a+b) > 0)
    {
        panic['PI_STATE']  = 1;
        panic['PI_HEALTH'] = 100;
        panic['PI_WAITER'] = 0;
    }
}
 function getOpenProcHealth(){
    if(tempData['SEND_OPENPROC'].reduce((a,b)=>a+b) > 0){
        const balanse = tempData['SEND_OPENPROC'].reduce((a,b)=>a+b)  - tempData['RESULT_OPENPROC'].reduce((a,b)=>a+b);
        if(balanse<=0)
        {
            panic['OPENPROC_HEALTH'] = 100 
            panic['OPENPROC_WAITER'] = 0
        } 
        else
        {
            panic['OPENPROC_HEALTH'] = 100 - Math.round(100*Math.abs(balanse)/tempData['SEND_OPENPROC'].reduce((a,b)=>a+b));
        }  
        if(panic['OPENPROC_HEALTH']<=10)
        {
            panic['OPENPROC_WAITER'] = panic['OPENPROC_WAITER'] +1
            if(panic['OPENPROC_WAITER']>3)
            {
                panic['OPENPROC_STATE']  = 0;
            }
        } 
        else
        {
            panic['OPENPROC_STATE']  = 1;
            panic['OPENPROC_WAITER'] = 0;
        }   
    }
    else if(tempData['RESULT_OPENPROC'].reduce((a,b)=>a+b) > 0)
    {
        panic['OPENPROC_STATE']  = 1;
        panic['OPENPROC_HEALTH'] = 100;
        panic['OPENPROC_WAITER'] = 0;
    }
}


 function getConfirmHealth(){
     
    if(tempData['CONFIRM'].reduce((a,b)=>a+b) > 0){
        const sendOut = tempData['SEND_OPENPROC'].reduce((a,b)=>a+b) - tempData['SEND_PI'].reduce((a,b)=>a+b) - tempData['EK_FIRST_DECL'].reduce((a,b)=>a+b)  - tempData['IMP_FIRST_DECL'].reduce((a,b)=>a+b) - tempData['TRANSIT_SEND'].reduce((a,b)=>a+b)- tempData['TRANSIT_PTD_SEND'].reduce((a,b)=>a+b);
        const balanse = sendOut - tempData['CONFIRM'].reduce((a,b)=>a+b);
        if(balanse<=0)
        {
            panic['CONFIRM_HEALTH'] = 100;
            panic['CONFIRM_WAITER']  = 0;
        } else
        {
            panic['CONFIRM_HEALTH'] = 100-Math.round(100*Math.abs(balanse)/sendOut);

        }
        if(panic['CONFIRM_HEALTH']<=10)
        {
            panic['CONFIRM_WAITER']  = panic['CONFIRM_WAITER']  + 1;
            if(panic['CONFIRM_WAITER']>4)
            {
                panic['CONFIRM_STATE']  = 0;
            }
        } 
        else
        {
            panic['CONFIRM_STATE']  = 1;
            panic['CONFIRM_WAITER']  = 0;
        }  
        
    } else{
        panic['CONFIRM_STATE']  = 1;
        panic['CONFIRM_HEALTH'] = 100;
        panic['CONFIRM_WAITER']  = 0;
        
    } 
}


function getGatewayHealth(){
     
    if(tempData['GATEWAY_OK'].reduce((a,b)=>a+b) > 0){
        const sendOut = tempData['SEND_OPENPROC'].reduce((a,b)=>a+b) - tempData['SEND_PI'].reduce((a,b)=>a+b) - tempData['EK_FIRST_DECL'].reduce((a,b)=>a+b)  - tempData['IMP_FIRST_DECL'].reduce((a,b)=>a+b) - tempData['TRANSIT_SEND'].reduce((a,b)=>a+b) - tempData['TRANSIT_PTD_SEND'].reduce((a,b)=>a+b);
        const balanse = sendOut - tempData['GATEWAY_OK'].reduce((a,b)=>a+b);
        if(balanse<=0)
        {
            panic['GATEWAY_HEALTH'] = 100;
            panic['GATEWAY_WAITER']  = 0;
        } else
        {
            panic['GATEWAY_HEALTH'] = 100-Math.round(100*Math.abs(balanse)/sendOut);

        }
        if(panic['GATEWAY_HEALTH']<=10)
        {
            panic['GATEWAY_WAITER']  = panic['GATEWAY_WAITER']  + 1;
            if(panic['GATEWAY_WAITER']>4)
            {
                panic['GATEWAY_STATE']  = 0;
            }
        } 
        else
        {
            panic['GATEWAY_STATE']  = 1;
            panic['GATEWAY_WAITER']  = 0;
        }  
        
    } else{
        panic['GATEWAY_STATE']  = 1;
        panic['GATEWAY_HEALTH'] = 100;
        panic['GATEWAY_WAITER']  = 0;
        
    } 
}

 function getDeclReceiveHealth(){

    const pervich = tempData['EK_FIRST_DECL'].reduce((a,b)=>a+b) + tempData['IMP_FIRST_DECL'].reduce((a,b)=>a+b);
    if(pervich > 0){
        const balanse = pervich - tempData['CUSTOMS_RECEICVE_DECL'].reduce((a,b)=>a+b);
        if(balanse<=0)
        {
            panic['DECLRECEIVE_HEALTH'] = 100 
            panic['DECLRECEIVE_WAITER'] = 0
        } 
        else
        {
            panic['DECLRECEIVE_HEALTH'] = 100 - Math.round(100*Math.abs(balanse)/pervich);
        }  
        if(panic['DECLRECEIVE_HEALTH']<=10)
        {
            panic['DECLRECEIVE_WAITER']  = panic['DECLRECEIVE_WAITER'] +1
            if(panic['DECLRECEIVE_WAITER'] > 5)
            {
                panic['DECLRECEIVE_STATE']  = 0;
            }
        } 
        else
        {
            panic['DECLRECEIVE_STATE']  = 1;
            panic['DECLRECEIVE_WAITER']  = 0;
        }   
    }
    else if(tempData['RESULT_OPENPROC'].reduce((a,b)=>a+b) > 0)
    {
        panic['DECLRECEIVE_STATE']  = 1;
        panic['DECLRECEIVE_HEALTH'] = 100;
        panic['DECLRECEIVE_WAITER']  = 0;
    }

    console.log('w' + panic['DECLRECEIVE_WAITER']);
    console.log('h' + panic['DECLRECEIVE_HEALTH']);
    console.log('s' + panic['DECLRECEIVE_STATE']);
}


 function getTDHealth(){
    const TDsm = tempData['TRANSIT_SEND'].reduce((a,b)=>a+b) + tempData['TRANSIT_PTD_SEND'].reduce((a,b)=>a+b);
    if(TDsm > 0){
        const balanse = TDsm  - tempData['TRANSIT_UIN'].reduce((a,b)=>a+b);
        if(balanse<=0)
        {
            panic['TRANSIT_HEALTH'] = 100 
            panic['TRANSIT_WAITER'] = 0
        } 
        else
        {
            panic['TRANSIT_HEALTH'] = 100 - Math.round(100*Math.abs(balanse)/TDsm);
        }  
        if(panic['TRANSIT_HEALTH']<=10)
        {
            panic['TRANSIT_WAITER'] = panic['TRANSIT_WAITER'] +1;
            if(panic['TRANSIT_WAITER']>3)
            {
                panic['TRANSIT_STATE']  = 0;
            }
        } 
        else
        {
            panic['TRANSIT_STATE']  = 1;
            panic['TRANSIT_WAITER'] = 0
        }   
    }
    else if(tempData['TRANSIT_UIN'].reduce((a,b)=>a+b) > 0)
    {
        panic['TRANSIT_STATE']  = 1;
        panic['TRANSIT_HEALTH'] = 100;
        panic['TRANSIT_WAITER'] = 0
    }
}


  