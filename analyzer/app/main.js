var request = require('request');
var iconv  = require('iconv-lite');
const cheerio = require('cheerio')
const fs = require('fs');
var Datastore = require('nedb');
var sourceDict = []
let raw_data = fs.readFileSync('./conf/config.json');
let config = JSON.parse(raw_data);

var mode_panic= require('./mode_panic');


const parser =  (err, res, body) => {
    var storeData = { }
    const text = cheerio.load(iconv.decode(body, 'koi8-r'))
    const txt = text('textarea').text() 
    for (const key in config["templates"]){
        if(config["templates"].hasOwnProperty(key)){
            var match = txt.match(config["templates"][key])
            if((key in sourceDict) == false)
            {
                sourceDict[key] = match[3]
            } else{
                storeData[key] = match[3] - sourceDict[key]
                sourceDict[key] = match[3]
                storeData[key+'_CNT'] = match[3] 
            }
        }
    }

    if(Object.keys(storeData).length === 0) return;

    mode_panic.getHealth(storeData);
    var db_ed = new Datastore({filename : process.env.DATA_PATH + '/ed'});
    db_ed.loadDatabase();
    storeData.date = new Date();
    db_ed.insert(storeData); 
}



function intervalFunc() {
    request(
        {url:'http://infotech.ed2inteh.ru/EDControl/show_ocstats.php', encoding: null}, 
        parser
       )
       let date_ob = new Date();
       let hours = date_ob.getHours();
       let minutes = date_ob.getMinutes();
       if(hours == 5 && minutes==11)
       {
        let path = process.env.DATA_PATH +'/ed'
        if (fs.existsSync(path)) {
            fs.unlink(path)
        }

        let path2 = process.env.DATA_PATH +'/ydet'
        if (fs.existsSync(path2)) {
            fs.unlink(path2)
        }
       }
  }
  
intervalFunc();
setInterval(intervalFunc, 60000);




